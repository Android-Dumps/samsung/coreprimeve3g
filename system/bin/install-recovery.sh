#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/sdio_emmc/by-name/RECOVERY:14381056:772d9ad14cb17cc0a106e24d85a28cbb47b29f21; then
  applypatch EMMC:/dev/block/platform/sdio_emmc/by-name/KERNEL:13649920:069c112d369dc144c16735f880b0c142696ca617 EMMC:/dev/block/platform/sdio_emmc/by-name/RECOVERY 772d9ad14cb17cc0a106e24d85a28cbb47b29f21 14381056 069c112d369dc144c16735f880b0c142696ca617:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
